# my harbor
基于[Harbor](https://github.com/vmware/harbor)提供docker registry service。
一方面它可以作为私有镜像存储服务，另一方面也可以作为 docker hub 的 mirror 服务器，用于缓存基础镜像。

### Tools
* [docker](https://github.com/moby/moby/releases)
* [docker-compose](https://github.com/docker/compose/releases)

### Setup
* 安装harbor需要的镜像
```
  docker load -i images/harbor.v1.1.1
```

* 启动 harbor
```
  docker-compose up -d
```

* 检查harbor运行状态
```
  docker-compose ps
```

* 停止harbor服务
```
  docker-compose stop
  # Or
  docker-compose down
```
